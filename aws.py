#working s3 / athena query with 
#access via athena

# in addition to .env file
SCHEMA_NAME = "mh-athena-db-twins"
S3_STAGING_DIR = "s3://ds.markh.twins.data2802/temp/"
QueryString = "select * from raw_data"
S3_BUCKET_NAME = "ds.markh.twins.data2802"
S3_OUTPUT_DIRECTORY = "temp"
temp_file_location: str = "athena_query_results.csv"


import os
import boto3
from time import sleep

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

# Database (mysql) variables
AWS_ACCESS_KEY = os.getenv('aws_access_key_id')
AWS_SECRET_KEY = os.getenv('aws_secret_access_key')
AWS_REGION = os.getenv('aws_region')
AWS_SESSION_TOKEN = os.getenv('aws_session_token')
# S3_STAGING_DIR = os.getenv('s3_staging_dir')

# see the results
print(AWS_ACCESS_KEY)
print(AWS_SECRET_KEY)
print(AWS_REGION)

# get a list of all buckets for the account
s3 = boto3.resource(
    's3',
    aws_access_key_id = AWS_ACCESS_KEY,
    aws_secret_access_key = AWS_SECRET_KEY,
    region_name = AWS_REGION,
	aws_session_token = AWS_SESSION_TOKEN,
)
# Print out bucket names
for bucket in s3.buckets.all():
    print(bucket.name)


# now connect to athena
athena_client = boto3.client(
    "athena",
    aws_access_key_id = AWS_ACCESS_KEY,
    aws_secret_access_key = AWS_SECRET_KEY,
    region_name = AWS_REGION,
	aws_session_token = AWS_SESSION_TOKEN,
)

# start the query
query_response = athena_client.start_query_execution(
    QueryString,
    QueryExecutionContext={"Database": SCHEMA_NAME},
    ResultConfiguration={
        "OutputLocation": S3_STAGING_DIR,
        "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
    },
)
# wait for the query to complete
while True:
    try:
        # This function only loads the first 1000 rows
        athena_client.get_query_results(
            QueryExecutionId=query_response["QueryExecutionId"]
		    )
        break
    except Exception as err:
        if "not yet finished" in str(err):
            sleep(0.001)
        else:
            raise err

# the query execution ID is used to create the output in output location
# show the 
print(query_response["QueryExecutionId"])
			

s3_client = boto3.client(
    "s3",
    aws_access_key_id = AWS_ACCESS_KEY,
    aws_secret_access_key = AWS_SECRET_KEY,
    region_name = AWS_REGION,
	aws_session_token = AWS_SESSION_TOKEN,
)
s3_client.download_file(
    S3_BUCKET_NAME,
    f"{S3_OUTPUT_DIRECTORY}/{query_response['QueryExecutionId']}.csv",
    temp_file_location,
)

import pandas as pd
pd.read_csv(temp_file_location)